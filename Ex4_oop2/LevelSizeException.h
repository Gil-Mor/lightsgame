#pragma once
#include <stdexcept>

class LevelSizeException :
    public std::runtime_error
{
public:
    LevelSizeException(const std::string& s);
    ~LevelSizeException();
};

