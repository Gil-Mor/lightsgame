#include "Board.h"
#include <math.h>
#include "Logger.h"
#include <math.h>
#include <boost\graph\breadth_first_search.hpp>
#include "BFSVisitor.h"
#include "BoardToVisitorProxy.h"


const sf::Color EDGE_COLOR = sf::Color(100, 100, 100, 255 );
const sf::Color UNLIGHT_COLOR = sf::Color(204, 0, 100, 255 );
const sf::Color LIGHT_COLOR = sf::Color(255, 215, 0, 255 );

Board::Board(std::pair<vector<vertex_p>, Graph> b)
    : _visitor(BoardToVisitorProxy(*this)),
    _vertices(std::move(b.first)), _graph(std::move(b.second))
{


    _circle.setRadius(VERTEX_RADIUS);
    _circle.setFillColor(UNLIGHT_COLOR);

    _rectEdges.resize(MAX_EDGES);
    sf::RectangleShape r({ EDGE_WIDTH, EDGE_HEIGHT });

    // set origin x coordinate to half the width of the rectangle so it rotates on it's
    // edge center and not it's top left corner.
    r.setOrigin({ EDGE_WIDTH/2, 0 });
    r.setFillColor(EDGE_COLOR);

    // set the first edge to point 9 o'clock where edge 0 should be.
    r.setRotation(90);
    
    for (size_t e = 0; e < MAX_EDGES; ++e)
    {    
        _rectEdges[e] = r;
        r.rotate(60);

    }
    examineVertices();
}


Board::~Board()
{
}


void Board::examineVertices()
{

    for (vertex_p& v : _vertices)
    {
        v->light(false);
        v->examineNeighbors(_graph);
        _allConnected = bfs();
    }
}

bool Board::bfs()
{
    vertex_d s = _vertices[_vertices.size() / 2]->getDescriptor();

    boost::breadth_first_search(_graph, s, visitor(BFSVisitor(BoardToVisitorProxy(*this))));
    
    for (const vertex_p& v : _vertices) {
        if (!v->getLight())
            return false;
    }
    return true;

}

void Board::lightVertex(size_t vertex)
{
    _vertices[vertex]->light(true);
}


void Board::handleClick(sf::Vector2f mouse, Vertex::Rotate dir)
{
    for (vertex_p& v : _vertices)
    {
        if (mouseOnVertex(*v, mouse))
        {
            v->rotate(dir);
            v->examineNeighbors(_graph);

            examineVertices();
            break;
        }
    }

}



bool Board::mouseOnVertex(const Vertex& v, sf::Vector2f mouse)
{
    // + radius to go from the top left corner to the center of the circle.
    return std::abs(v.getSfmlPos().x + VERTEX_RADIUS - mouse.x) < 10
        && std::abs(v.getSfmlPos().y + VERTEX_RADIUS - mouse.y) < 10;

}

void Board::draw(sf::RenderWindow& window)
{


    for (const vertex_p& v : _vertices)
    {
        drawEdges(*v, window);


        _circle.setPosition(v->getSfmlPos());

        if (v->getLight())
            _circle.setFillColor(LIGHT_COLOR);
        else
            _circle.setFillColor(UNLIGHT_COLOR);
        window.draw(_circle);
    }
}

void Board::drawEdges(const Vertex& v, sf::RenderWindow& window)
{


    sf::Vector2f center = v.getSfmlPos();
    for (size_t e = 0; e < MAX_EDGES; ++e)
    {
        _rectEdges[e].setPosition(center + sf::Vector2f(VERTEX_RADIUS, VERTEX_RADIUS));
        //_convexEdges[e].setPosition(center + sf::Vector2f(VERTEX_RADIUS, VERTEX_RADIUS));
        //_convexEdges[e].setOrigin(center + sf::Vector2f(VERTEX_RADIUS, VERTEX_RADIUS));


        if (v.edgeExist(e)) {
            window.draw(_rectEdges[e]);
        }

    }

}

bool Board::win()
{
    return _allConnected;
}