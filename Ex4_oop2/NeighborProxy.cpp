#include "NeighborProxy.h"
#include "Vertex.h"


NeighborProxy::NeighborProxy(const Vertex* me, size_t neighbor)
    :_me(me), _neighbor(neighbor)
{
}

NeighborProxy::NeighborProxy(const NeighborProxy& rhs)
    : _me(rhs._me), _neighbor(rhs._neighbor)
{

}

NeighborProxy::~NeighborProxy()
{
}


bool NeighborProxy::edgeExist() const
{
    return _me->edgeExist(_neighbor);
}

const vertex_d& NeighborProxy::getNeighborDescriptor() const
{
    return _me->getDescriptor();
}

NeighborProxy& NeighborProxy::operator=(const NeighborProxy& rhs)
{
    if (&rhs == this)
        return *this;

    this->_me = rhs._me;
    this->_neighbor = rhs._neighbor;
    return *this;

}
