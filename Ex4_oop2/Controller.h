#pragma once
#include <SFML\Graphics.hpp>
#include "Board.h"
#include "definitions.h"
#include <string>
using std::string;

class Controller
{
public:

    enum State
    {
        win_s,
        finish_s,
        exiting_s
    };
    Controller();
    ~Controller();

    void run();


private:

    // indicates that the Controller was successfully built.
    bool _buildSuccess = true;

    size_t _currLevel = 0;

    // num of levels is set to 3 by default but can be updated through 'LevelBuilder'
    size_t _numOfLevels;

    sf::RenderWindow _window;
    void initWindow();

    State _state = win_s;

    std::pair<vector<vertex_p>, Graph> loadNextBoard();

    void finishMessage();

    void gameLoop(Board& board);

    void showMessage(const string& message);


};

