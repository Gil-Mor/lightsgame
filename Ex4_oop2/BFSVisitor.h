#pragma once
#include "definitions.h"
#include <boost\graph\breadth_first_search.hpp>
class BoardToVisitorProxy;

class BFSVisitor : public boost::default_bfs_visitor
{
public:

    BFSVisitor(BoardToVisitorProxy& boardProxy);

    template < typename Vertex, typename Graph >
    void discover_vertex(Vertex u, const Graph& g) 
    {
        lightVertex(size_t(u));
    }

    void lightVertex(size_t vertex);

private:
    BoardToVisitorProxy& _boardProxy;
};

