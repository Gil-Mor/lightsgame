#pragma once
#include <stdexcept>
#include <string>
class EdgeLineException : public std::runtime_error
{
public:
    EdgeLineException(const std::string& s);
};

