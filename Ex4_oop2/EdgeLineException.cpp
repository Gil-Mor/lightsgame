#include "EdgeLineException.h"

const std::string message = "illegal Edges line in file. ";

EdgeLineException::EdgeLineException(const std::string& s)
    :std::runtime_error(message + s)
{
}

