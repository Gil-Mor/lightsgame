#pragma once
#include <string>
#include <fstream>
#include <memory>

using std::unique_ptr;
using std::ofstream;
using std::string;

class Logger
{
public:
    static Logger* getInstance();

    void log(const string& message);

    ~Logger();

private:

    static unique_ptr<Logger> _instance;

    ofstream file;

    size_t _errors = 0;

    Logger();
    Logger(const Logger&);
    Logger& operator=(Logger const& copy);
};

