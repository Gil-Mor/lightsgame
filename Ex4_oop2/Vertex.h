#pragma once
#include <vector>
#include <SFML\Graphics.hpp>
#include "NeighborProxy.h"
#include <deque>  // for bool array instead of vector. vector<bool> is broken.
#include <memory>
#include "definitions.h"


using std::unique_ptr;
using std::deque;
using std::vector;

typedef std::pair<NeighborProxy, bool> Neighbor;

class Vertex
{
public:

    enum Rotate
    {
        clockwise,
        c_clockwise
    };

    Vertex();
    ~Vertex();

    void light(bool l);
    bool getLight() const;

    void rotate(Rotate);

    bool edgeExist(size_t edge) const;

    void examineNeighbors(Graph& graph);

    NeighborProxy getProxy(size_t neighbor) const;

    void setDescriptor(vertex_d& vd);
    vertex_d getDescriptor() const;

    void setSfmlPos(sf::Vector2f pos);

    sf::Vector2f getSfmlPos() const;

    // only used by LevelBuilder to build the vertex.
    // sets the vertex edges from the file.
    void setEdge(size_t edge);

    // only used by LevelBuilder to build the vertex.
    // sets the vertex neighbors as specified in the file.
    void setNeighbor(NeighborProxy neighbor, size_t index);

    void info();


private:

    // boost graph vertex descriptor. 
    vertex_d _vertex_d;

    // vector bool is not so good....
    deque<bool> _edges;
    deque<bool> _connectedNeighbors;

    bool _light = false;

    // Neighbor proxy examines the right edge that connects between the two
    // the bool says if the neighbor exists.
    vector<Neighbor> _neighbors;

    vector<NeighborProxy> _proxies;

    sf::Vector2f _pos;

};

