#include "BFSVisitor.h"
#include "BoardToVisitorProxy.h"

BFSVisitor::BFSVisitor(BoardToVisitorProxy& boardProxy)
    :_boardProxy(boardProxy)
{}

void BFSVisitor::lightVertex(size_t vertex)
{
    _boardProxy.lightVertex(vertex);
}


