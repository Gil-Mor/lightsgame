#include "Controller.h"
#include "LevelBuilder.h"
#include "Logger.h"
#include "Board.h"
#include <SFML\Graphics.hpp>
#include "definitions.h"
#include <vector>
#include <memory>



using std::vector;
using std::unique_ptr;



Controller::Controller()
{
    try {
        _numOfLevels = LevelBuilder::getInstance()->getNumOfLevels();
    }
    catch (std::exception& e) {
        _buildSuccess = false;
    }


    if (_numOfLevels == 0)
    {
        _buildSuccess = false;
    }

    initWindow();

}

void Controller::initWindow()
{
    _window.create(sf::VideoMode(unsigned(WINDOW_SIZE), unsigned(WINDOW_SIZE)), "LIGHTUSH");
    _window.setFramerateLimit(50);
}
Controller::~Controller()
{
}

void Controller::run()
{
    if (!_buildSuccess) 
    {
        Logger::getInstance()->log("Error with LevelBuilder. Controller couldn't get levels.");
        throw std::runtime_error("Error with LevelBuilder.");
    }

    try
    {
        
        while (_state != exiting_s && _state != finish_s)
        {
            Board board(loadNextBoard());

            gameLoop(board);

            if (_state == exiting_s) {
                break;
            }
            if (_currLevel >= _numOfLevels) {
                _state = finish_s;
            }

            if (_state == win_s) {
                showMessage("You've won. Move on to the next level");
            }

        }


        // if the user finished all levels show the finish message and then quit.
        // otherwise just quit.
        if (_state == finish_s) {
            showMessage("You've finished all levels!. That's it...");
            _state = exiting_s;
        }

        _window.close();
    }





    catch (std::exception& e) {
        throw;
    }

}



void Controller::gameLoop(Board& board)
{
    while (true)
    {
        sf::Event event;
        sf::Vector2f mouse = _window.mapPixelToCoords(sf::Mouse::getPosition(_window));

        while (_window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
            {
                _state = exiting_s;
                return;
            }

            if (event.type == sf::Event::MouseButtonPressed)
            {
                if (event.mouseButton.button == sf::Mouse::Left) {
                    board.handleClick(mouse, Vertex::Rotate::clockwise);
                }

                if (event.mouseButton.button == sf::Mouse::Right) {
                    board.handleClick(mouse, Vertex::Rotate::c_clockwise);
                }
            }

            
        }

        _window.clear();
        board.draw(_window);
        _window.display();

        if (board.win()) {
            _state = win_s;
            board.draw(_window);
            return;
        }
    }
}

std::pair<vector<vertex_p>, Graph> Controller::loadNextBoard()
{
    try {
        return LevelBuilder::getInstance()->getLevel(_currLevel++);
    }
    catch (std::exception& e) {
        Logger::getInstance()->log("couldn't get level " + std::to_string(_currLevel) + " exiting.");
        throw;
    }

    
}

void Controller::showMessage(const string& message)
{

    sf::Text text;
    sf::Font font;
    text.setStyle(sf::Text::Regular);

    text.setPosition(sf::Vector2f(_window.getSize().x / 2.f, _window.getSize().y / 2.f));

    font.loadFromFile(FONT_FILE_NAME);

    text.setFont(font);

    text.setColor(sf::Color::Blue);

    text.setString(message);

    text.setCharacterSize(30);

    sf::Vector2f center;

    sf::FloatRect textSize;

    textSize = text.getGlobalBounds();

    // make the center of text it's origin.
    center = { textSize.width / 2.f, textSize.height / 2.f };

    text.setOrigin(center);


    _window.draw(text);

    _window.display();

    sf::Event event;

    while (true)
    {
        while (_window.pollEvent(event))
        {
            switch (event.type)
            {
                case sf::Event::Closed:
                    _state = exiting_s;
                case sf::Event::KeyPressed:
                case sf::Event::MouseButtonPressed:
                    return;
            }
        }
    }
}

