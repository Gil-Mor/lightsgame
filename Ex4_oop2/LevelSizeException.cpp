#include "LevelSizeException.h"

const std::string message = "illegal level size line in file. ";

LevelSizeException::LevelSizeException(const std::string& s)
    :std::runtime_error(message + s)
{
}


LevelSizeException::~LevelSizeException()
{
}
