#pragma once
#include <boost\graph\adjacency_list.hpp>
#include <boost\graph\graph_traits.hpp>
#include <string>

const unsigned MAX_EDGES = 6;
const unsigned MIN_EDGES = 0;

const float WINDOW_SIZE = 600;

const float VERTEX_RADIUS = 10;

const float EDGE_WIDTH = 2;
const float EDGE_HEIGHT = 25;

typedef boost::adjacency_list < boost::vecS, boost::vecS, boost::undirectedS > Graph;

typedef boost::graph_traits<Graph>::vertex_descriptor vertex_d;

const std::string FONT_FILE_NAME = "../font/impact.ttf";