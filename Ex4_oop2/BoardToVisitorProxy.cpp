#include "BoardToVisitorProxy.h"
#include "Board.h"

BoardToVisitorProxy::BoardToVisitorProxy(Board& board)
    : _board(board)
{
}


BoardToVisitorProxy::~BoardToVisitorProxy()
{
}

void BoardToVisitorProxy::lightVertex(size_t vertex)
{
    _board.lightVertex(vertex);
}
