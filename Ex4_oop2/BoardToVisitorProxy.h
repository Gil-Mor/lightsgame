#pragma once

class Board;
class BoardToVisitorProxy
{
public:
    BoardToVisitorProxy(Board& board);
    ~BoardToVisitorProxy();

    void lightVertex(size_t vertex);

private:
    Board& _board;
};

