#include "Vertex.h"
#include <algorithm>
#include "NeighborProxy.h"


Vertex::Vertex()
{

    for (size_t i = 0; i < MAX_EDGES; ++i) 
    {
        _proxies.emplace_back(NeighborProxy(this, i));
    }

    _edges.resize(MAX_EDGES);
    std::fill(_edges.begin(), _edges.end(), false);

    _connectedNeighbors.resize(MAX_EDGES);
    std::fill(_connectedNeighbors.begin(), _connectedNeighbors.end(), false);

    // for simplicity. every vertex has 6 potential neighbors
    for (size_t i = 0; i < MAX_EDGES; ++i){
        _neighbors.push_back(std::make_pair(NeighborProxy(nullptr, 0), false));
    }

}


Vertex::~Vertex()
{
}


void Vertex::setDescriptor(vertex_d& vd)
{
    _vertex_d = vd;
}

NeighborProxy Vertex::getProxy(size_t neighbor) const
{
    return _proxies[neighbor];
}

void Vertex::examineNeighbors(Graph& graph)
{

    for (size_t n = 0; n < _neighbors.size(); ++n)
    {
        // if neighbor exist
        if (_neighbors[n].second == true)
        {

            // if my edge exist and the nth neighbor's corresponding edge exists
            // so we are connected.
            if (_edges[n]) {
                if (_neighbors[n].first.edgeExist()) {
                    _connectedNeighbors[n] = true;

                    // if this edge doesn't exist in the graph so add it
                    if (!boost::edge(_vertex_d, _neighbors[n].first.getNeighborDescriptor(), graph).second) {
                        boost::add_edge(_vertex_d, _neighbors[n].first.getNeighborDescriptor(), graph);
                    }
                }
            }

            else {
                _connectedNeighbors[n] = false;

                if (boost::edge(_vertex_d, _neighbors[n].first.getNeighborDescriptor(), graph).second) {
                    boost::remove_edge(_vertex_d, _neighbors[n].first.getNeighborDescriptor(), graph);
                }
            }
        }
        else {
            _connectedNeighbors[n] = false;
        }

    }

}



void Vertex::setNeighbor(NeighborProxy neighbor, size_t index)
{
    // neighborNum is relative to my neighbors numbering.
    // neighbor has my num relative to his numbering.
    // for example, I need to examine the 5th edge of my 1th neighbor.
    // and he needs to do the opposite.

    _neighbors[index].first = neighbor;
    _neighbors[index].second = true;
}

bool Vertex::edgeExist(size_t edge) const
{
    return _edges[edge];
}

void Vertex::setSfmlPos(sf::Vector2f pos)
{
    _pos = pos;
}

sf::Vector2f Vertex::getSfmlPos() const
{
    return _pos;
}

void Vertex::rotate(Rotate dir)
{
    // clockwise rotate. make the last element the first.
    if (dir == clockwise) {
        std::rotate(_edges.rbegin(), _edges.rbegin()+1, _edges.rend());
    }

    // counter clockwise. make the second element the first (reverse).
    else {
        std::rotate(_edges.begin(), _edges.begin() + 1, _edges.end());
    }

}

void Vertex::setEdge(size_t edge)
{
    if (edge < MIN_EDGES || edge >= MAX_EDGES)
        return;
    _edges[edge] = true;

}

vertex_d Vertex::getDescriptor() const
{
    return _vertex_d;
}


void Vertex::light(bool l)
{
    _light = l;
}

bool Vertex::getLight() const
{
    return _light;
}