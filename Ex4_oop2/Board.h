#pragma once
#include "Vertex.h"
#include <vector>
#include <SFML\Graphics.hpp>
#include <memory>
#include "Vertex.h"
#include <boost\graph\adjacency_list.hpp>
#include "definitions.h"
#include "BFSVisitor.h" 


using std::unique_ptr;
using std::vector;

typedef unique_ptr<Vertex> vertex_p;
//typedef boost::adjacency_list < boost::vecS, boost::vecS, boost::undirectedS > Graph;

class Board
{
public:

    Board(std::pair<vector<vertex_p>, Graph> b);
    ~Board();

    void lightVertex(size_t vertex);
    void handleClick(sf::Vector2f, Vertex::Rotate);

    bool win();
    
    void draw(sf::RenderWindow& window);

    // delete copy constructor because Board contains an array of unique_ptrs.
    Board(const Board&) = delete;
    Board& operator=(const Board&) = delete;

private:
    vector<vertex_p > _vertices;
    Graph _graph;

    void examineVertices();

    bool bfs();

    BFSVisitor _visitor;

    // use the same circle for drawing.
    sf::CircleShape _circle;


    // every bfs updated this boolean to true if all vertices are connected,
    // and false otherwise.
    bool _allConnected = false;

    bool mouseOnVertex(const Vertex& v, sf::Vector2f mouse);

    void Board::drawEdges(const Vertex& v, sf::RenderWindow& window);

    vector<sf::RectangleShape> _rectEdges;
};

