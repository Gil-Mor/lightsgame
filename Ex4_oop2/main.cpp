                  
#include "Controller.h"
#include "Logger.h"
#include <SFML\Graphics.hpp>
#include "definitions.h"  // for font
#include <string>

void showErrorMessage(const string& message);

int main() try
{
    try {
        Controller c;
        c.run();
    }
    catch (std::exception& e) {
        showErrorMessage("Error initializing the game.\nSee log.txt file in project folder for details.\nExiting\n");
        throw;
    }
    return 0;
}

catch (std::exception& e) {
    Logger::getInstance()->log("main threw an Exception. Exiting.");
}

void showErrorMessage(const string& message)
{
    sf::RenderWindow window;
    window.create(sf::VideoMode(unsigned(WINDOW_SIZE), unsigned(WINDOW_SIZE)), "LIGHTUSH");
    window.setFramerateLimit(50);

    sf::Text text;
    sf::Font font;
    text.setStyle(sf::Text::Regular);

    text.setPosition(sf::Vector2f(window.getSize().x / 2.f, window.getSize().y / 2.f));

    font.loadFromFile(FONT_FILE_NAME);

    text.setFont(font);

    text.setColor(sf::Color::Blue);

    text.setString(message);

    text.setCharacterSize(30);

    sf::Vector2f center;

    sf::FloatRect textSize;

    textSize = text.getGlobalBounds();

    // make the center of text it's origin.
    center = { textSize.width / 2.f, textSize.height / 2.f };

    text.setOrigin(center);


    window.draw(text);

    window.display();

    sf::Event event;

    while (true)
    {
        while (window.pollEvent(event))
        {
            switch (event.type)
            {
                case sf::Event::Closed:
                case sf::Event::KeyPressed:
                case sf::Event::MouseButtonPressed:
                    return;
            }
        }
    }
}