#pragma once
#include "definitions.h"

class Vertex;

class NeighborProxy
{
public:

    NeighborProxy(const Vertex* me, size_t neighbor);
    NeighborProxy(const NeighborProxy& rhs);

    ~NeighborProxy();

    const vertex_d& getNeighborDescriptor() const;

    bool edgeExist() const;

    NeighborProxy& operator=(const NeighborProxy& rhs);
private:
    const Vertex* _me = nullptr;
    size_t _neighbor;
};

