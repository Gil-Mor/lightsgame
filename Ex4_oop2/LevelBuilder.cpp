

#include "LevelBuilder.h"
#include <fstream>
#include <stdexcept>
#include "Logger.h"
#include <cmath>
#include <algorithm> // std::min
#include <memory>
#include <sstream>
#include "EdgeLineException.h"
#include "LevelSizeException.h"
#include "definitions.h"
#include <SFML\Graphics.hpp>

using std::stringstream;
using std::unique_ptr;
using std::ifstream;

unique_ptr<LevelBuilder> LevelBuilder::_instance;

const string LEVELS_DIR = "../levels/";
const size_t MIN_VER_IN_ROW = 3;

LevelBuilder::LevelBuilder()
{
    ifstream file;
    try {
        file = openFile(LEVELS_DIR + "levels.txt");
    }

    // don't throw from ctor.
    catch (std::exception& e) { 
        Logger::getInstance()->log("error opening levels file.");
        _buildSuccess = false;
    }

    if (_buildSuccess) {
        getLevels(file);
    }

    file.close();

}

void LevelBuilder::getLevels(ifstream& file)
{
    string line;

    try
    {
        while (!file.eof())
        {
            getline(file, line);
            if (!line.empty())
                _levels.push_back(LEVELS_DIR + line);
        }
    }

    // function called from ctor so don't throw...
    catch (std::exception& e)
    {
        Logger::getInstance()->log("error reading levels file. ");
        _buildSuccess = false;
        // nothrow!!!
    }
}

size_t LevelBuilder::getNumOfLevels() const
{
    return _levels.size();
}


std::pair<vector<vertex_p>, Graph> LevelBuilder::getLevel(size_t index)
{
    try
    {

        if (index < 0 || index >= _levels.size())
        {
            Logger::getInstance()->log("invalid level requested.\n requested: "
                + str(index) + " num of levels is: " + str(_levels.size()));

            throw std::invalid_argument(str(index));
        }

        ifstream file = openFile(_levels[index]);

        vector<vertex_p> board = std::move(buildLevel(file));
        Graph graph = makeBoostGraph(board);

        return std::make_pair(std::move(board), graph);
    }

    catch (std::exception& e) {
        throw;
    }



}

Graph LevelBuilder::makeBoostGraph(vector<vertex_p>& vertices)
{
    try {
        Graph graph;
        for (vertex_p& v : vertices)
        {
            vertex_d vd = boost::add_vertex(graph);
            v->setDescriptor(vd);
        }
        return graph;
    }
    catch (std::exception& e) {
        Logger::getInstance()->log("error while adding vertices to boost graph in level builder.");
        throw;
    }
}


vector<vertex_p> LevelBuilder::buildLevel(ifstream& file)
{
    try
    {
        size_t verInMaxLine = getLevelSize(file);

        Matrix matrix = prepareMatrix(verInMaxLine);
        
        initVertices(file, matrix);

        setSFMLPositions(matrix);

        vector<vertex_p> forBoard = std::move(matrixToVector(matrix));

        return std::move(forBoard);
    }

    catch (std::exception& e)
    {
        throw;
    }


}

void LevelBuilder::setSFMLPositions(Matrix& matrix)
{
    sf::Vector2f center(WINDOW_SIZE / 2, WINDOW_SIZE / 2);

    // the neighbors should form an Equilateral triangle
    sf::Vector2f gap(EDGE_HEIGHT*2, (sqrt(3) / 2)*EDGE_HEIGHT*2);

    int maxLine = ((matrix.size() - 1) / 2);

    sf::Vector2f firstPos(center.x - gap.x, center.y - gap.y*float(maxLine));

    sf::Vector2f pos = firstPos;

    for (size_t i = 0; i < matrix.size(); ++i)
    {
        for (size_t j = 0; j < matrix[i].size(); ++j)
        {
            if (matrix[i][j])
            {
                matrix[i][j]->setSfmlPos(pos);
                pos.x += gap.x;
            }
        }
        pos.y += gap.y;
        firstPos.x = i < maxLine ? firstPos.x - gap.x/2 : firstPos.x + gap.x/2;
        pos.x = firstPos.x;
    }
}


void LevelBuilder::initVertices(ifstream& file, Matrix& matrix)
{
    string line;
    try
    {
        int vertexNum = 0;
        for (size_t i = 0; i < matrix.size(); ++i)
        {
            for (size_t j = 0; j < matrix[i].size(); ++j)
            {
                if (matrix[i][j]) 
                {
                    try {
                        setEdges(file, *matrix[i][j]);
                        setNeighbors(matrix, *matrix[i][j], i, j);
                        vertexNum++;
                    }
                    catch (std::exception& e) {
                        Logger::getInstance()->log("error setting edges and neighbors for vertex: " + str(vertexNum));
                        throw;
                    }
                }
            }
        }
    }
    catch (std::exception& e) {
        throw;
    }
}

void LevelBuilder::setEdges(ifstream& file, Vertex& v)
{
    string line = getNextLine(file);

    // get rid of vertex indexes at the beginning of the line if they exist (2/3:).
    if (line.find(":") != string::npos)
        line = line.substr(line.find(":")+1, line.size());

    // check that the line contains only spaces, commas and positive numbers.
    if (!validEdgesLine(line)) {
        Logger::getInstance()->log("invalid edge line. line: " + line);
        throw EdgeLineException(line);
    }

    stringstream s(line);
    string num;
    while (s >> num) {
        int edge = std::stoi(num);
        if (edge < 0 || edge >= MAX_EDGES) {
            Logger::getInstance()->log("invalid edge number. needs to be between 0 and " + str(MAX_EDGES - 1)
                + " but was " + num);
            throw EdgeLineException(line);
        }
        v.setEdge(edge);
    }


}

void LevelBuilder::setNeighbors(Matrix& matrix, Vertex& v, size_t row, size_t col)
{

    // go around the vertex where it's neighbors should be. 
    // set the vertex neighbors by indexes. starting from the left one (neighbor 0)
    // and going clockwise. 
    // the number inside the 'NeighborProxy' is the edge that connects the neighbor to this
    // vertex. the right number outside NeighborProxy is this vertex edge that connects it to 
    // that neighbor.

    // n <- v 
    if (validIndex(matrix, row, col - 2)) {
        v.setNeighbor(matrix[row][col - 2]->getProxy(3), 0);
        //v.setNeighbor(new NeighborProxy(*matrix[row][col].get(), 3), 0);
    }



    if (validIndex(matrix, row - 1, col - 1)) {
        v.setNeighbor(matrix[row-1][col -1]->getProxy(4), 1);
        //v.setNeighbor(new NeighborProxy(*matrix[row][col].get(), 4), 1);
    }

    //  ^
    // /
    if (validIndex(matrix, row - 1, col + 1)) {
        v.setNeighbor(matrix[row-1][col +1]->getProxy(5), 2);

        //v.setNeighbor(new NeighborProxy(*matrix[row][col].get(), 5), 2);
    }

    // v-> n
    if (validIndex(matrix, row, col + 2)) {
        v.setNeighbor(matrix[row][col + 2]->getProxy(0), 3);

        //v.setNeighbor(new NeighborProxy(*matrix[row][col].get(), 0), 3);
    }

    // \
    //  v
    if (validIndex(matrix, row + 1, col + 1)) {
        v.setNeighbor(matrix[row+1][col + 1]->getProxy(1), 4);

        //v.setNeighbor(new NeighborProxy(*matrix[row][col].get(), 1), 4);
    }

    //  /
    // v
    if (validIndex(matrix, row + 1, col - 1)) {
        v.setNeighbor(matrix[row+1][col - 1]->getProxy(2), 5);

        //v.setNeighbor(new NeighborProxy(*matrix[row][col].get(), 2), 5);
    }

}

bool LevelBuilder::validIndex(Matrix& m, size_t row, size_t col)
{
    return (0 <= row && row < m.size())
        && (0 <= col && col < m[row].size())
        && m[row][col];
}


bool LevelBuilder::validEdgesLine(const string& s)
{
    // also count num of edges specified in the line.
    int numOfDigits = 0;
    for (char c : s) {
        if (isdigit(c)) {
            numOfDigits++;
        }
            
        if (!isspace(c) && !isdigit(c)) {
            return false;
        }
    }

    if (numOfDigits == 0 || numOfDigits > 6)
        return false;

    return true;
}


Matrix LevelBuilder::prepareMatrix(size_t verInMaxLine)
{
    size_t numOfRows = 2 * (verInMaxLine - MIN_VER_IN_ROW) + 1;
    Matrix vertices(numOfRows);
    

    // I'm making a sparse matrix where each row is the size of 2*levelsize -1
    // and where every vertex is separated by one column from the next. 
    // this will allow me to set the neighbors mor easily.
    size_t lineLength = verInMaxLine * 2 - 1;

    for (size_t i = 0; i < numOfRows; ++i)
    {
        vertices[i] = prepareLine(verInMaxLine, numOfRows, lineLength, i);
    }

    return vertices;
}

vector<unique_ptr<Vertex>> LevelBuilder::prepareLine(size_t verInMaxLine, size_t numOfRows, size_t lineLength, size_t i)
{

    vector<unique_ptr<Vertex>> line(lineLength);

    // the middle line with maximum vertices.
    int maxLineIndex = (numOfRows - 1) / 2;
    size_t verInLine = verInMaxLine - std::abs(int(maxLineIndex - i));


    size_t pos = std::abs(int(maxLineIndex - i));

    for (size_t x = 0; x < verInLine; ++x, pos += 2)
    {
        line[pos].reset(new Vertex);
    }
    return line;

}

vector<vertex_p > LevelBuilder::matrixToVector(Matrix& matrix)
{
    vector<vertex_p > v;
    for (size_t i = 0; i < matrix.size(); ++i)
    {
        for (size_t j = 0; j < matrix[i].size(); ++j)
        {
            if (matrix[i][j]) 
            {
                vertex_p p;
                p.swap(matrix[i][j]);
                v.push_back(std::move(p));
            }
        }
    }
    return std::move(v);
}

size_t LevelBuilder::getLevelSize(ifstream& file)
{
    string line = getNextLine(file);
    for (char c : line) {
        if (!isdigit(c) || c == ' ') {
            Logger::getInstance()->log("illegal char in line: " + str(c));
            throw LevelSizeException("illegal char in line: " + str(c));
        }
    }

    size_t size = std::stoi(line);
    if (size != 5 && size != 7 && size != 9) {
        Logger::getInstance()->log("illegal level size. should be 5, 7 or 9 but was: " + str(size));
        throw LevelSizeException("illegal level size. should be 5, 7 or 9 but was: " + str(size));
    }

    return size;
}

string LevelBuilder::getNextLine(ifstream& file)
{
    string line;
    try
    {
        do {
            getline(file, line);
        } while (line.empty());

        return line;
    }
    catch (std::exception& e) {
        throw;
    }
}




ifstream LevelBuilder::openFile(const string& s)
{
    ifstream file;
    file.open(s);
    if (!file.is_open())
    {
        Logger::getInstance()->log("couldn't open file. file name: " + s);
        throw std::invalid_argument(s);
    }
    return file;
}

LevelBuilder* LevelBuilder::getInstance()
{
    if (!_instance)
        _instance.reset(new LevelBuilder);
    return _instance.get();
}



