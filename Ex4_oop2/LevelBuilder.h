#pragma once
#include <vector>
#include <string>
#include <memory>
#include "Board.h"
#include <fstream>
#include "definitions.h"


using std::unique_ptr;
using std::vector;
using std::string;
using std::ifstream;

//typedef unique_ptr<Vertex> unique_ptr<Vertex>;

typedef vector<vector<vertex_p> > Matrix;

class LevelBuilder
{
public:
    static LevelBuilder* getInstance();

    std::pair<vector<vertex_p>, Graph> getLevel(size_t level);

    size_t getNumOfLevels() const;

private:

    // make the ctor and copy ctor private;
   


    static unique_ptr<LevelBuilder> _instance;

    vector<string> _levels;


    // indicates that the initial file reading was successful.
    bool _buildSuccess = true;

    void getLevels(ifstream& file);

    ifstream openFile(const string& s);

    vector<vertex_p> buildLevel(ifstream& file);

    Graph makeBoostGraph(vector<vertex_p>& v);

    string getNextLine(ifstream& file);
    size_t getLevelSize(ifstream& file);

    Matrix prepareMatrix(size_t maxLine);
    vector<vertex_p> prepareLine(size_t verInMaxLine, size_t numOfRows, size_t lineLength, size_t i);

    void initVertices(ifstream& file, Matrix& matrix);

    void setEdges(ifstream& file, Vertex& v);

    void setNeighbors(Matrix& matrix, Vertex& v, size_t row, size_t col);

    void setSFMLPositions(Matrix& matrix);

    bool validIndex(Matrix& m, size_t row, size_t col);

    bool validEdgesLine(const string& s);

    template<typename T>
    string str(const T&);

    vector<vertex_p> matrixToVector(Matrix& matrix);



    LevelBuilder();
    LevelBuilder(const LevelBuilder&);
    LevelBuilder& operator=(LevelBuilder const& copy);


};

template<typename T>
string LevelBuilder::str(const T& t)
{
    return std::to_string(t);
}