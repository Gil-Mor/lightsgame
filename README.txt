﻿1.----------- README ----------------

2. ---------- STUDENT --------------- 
Ex4 OOP Sem 2
Gil Mor גיל מור 300581030

3.-------------- THE EXERCISE -----------------

Create the 'lights' game.
Connect all the lights to the middle light and light them.
Click on the lights to rotate the edges.
Left Click to rotate clockwise, right click to rotate counter clockwise.



------ CREATING A LEVEL FILE ---------

the first line in the file needs to contain only one number (5, 7 or 9) which is the level size. 
It is the number of lights in the middle row.

After that, each line represents a vertex. The order is from left to right
and from top to bottom so the first line is the top left vertex
and the last line is the bottom right vertex.

each line needs to contain 1 to 6 numbers in range 0 to 5 which indicate
which edges that vertex will have. edge 0 points to 21:00 o'clock <-- 
1 points to 23:00 etc...

You can just write the numbers in each line separated by white-space
or you can write something before like the index of that vertex.
So the line for the third vertex in the second with edges 0, 2, and 4 row could be:
4 0 2
or it could be
2/3: 4 0 2
for your comfort..

IMPORTANT: if you write something before the edges you have to put ':' after it!!!

It can be easier to update your level when you see the vertex index in the file..

There are file templates for the three sizes in a folder called 'level File Templates'

After you create your file put it in 'levels' folder and add
it's full name to the 'levels.txt' file in that directory.
The name can be anything...
That's it.

See the levels already in 'levels' folder.


4.--------------- DESIGN -----------------


*** I use the term 'neighbors' for potential neighbors - two vertices that
    have the potential to be connected.
    two vertices are 'connected' if their edges are connected. 

--- Vertices ----
Each vertex has 6 potential neighbors and six potential edges.
The edges are bool array.
The neighbors are kept in vector of std::pair<NeighborProxy>, bool>.
The bool tells if there's a neighbor and NeighborProxy is used to communicate with the neighbor.
each vertex 6 proxies that it gives two potential neighbors.
If a vertex find that he is connected to a neighbor, he adds an edge
between them in the boost::graph he gets as reference from the Board.

--- NeighborProxy ---
A vertex gives it's own proxy to neighbors.
With the proxy, the neighbor can get the vertex boost Vertex descriptor
and also to check if the neighbor's corresponding edge exists.
if A is the neighbor 0'ht of B (in potential) - A is just to the left of B -   A   B
then A's 3'th edge needs to meet B's 0'th edge. So A has B's proxy with access to it's 0'th edge
and B has A's proxy with access to A's 3'th edge.. 

--- Level Builder ---
Is a singleton responsible for delivering boards (or levels) to the Controller.
Singleton because you would never want two objects building the same board twice..
First it reads the 'levels.txt' file and stores the levels file names in a vector.
then, each time, the Controller asks for the next board.
level builder also set's the initial boost graph.

--- Board Building ---
In order to easily find the neighbors for any board size
I'm making a sparse matrix where each row is the size of (2*level size) -1
and where every vertex is separated by one column from the next. 
this will allow me to set the neighbors more easily.
Because the vertices contain these Proxies with there own addresses,
VERTICES CAN'T BE COPIED AFTER CREATION. Their proxies will lose
the original vertex address and the game won't work. 
This led me to create the vertices as unique_ptr's and move them around.
After building the board (setting neighbors and edges) I move them to a vector..
So Because I create the vertices outside of the Board and use these proxies
I needed to use unique_ptr's...

--- Board ---
Gets a pair of vector of vertices and boost graph.
It gets SFML events from the Controller (just mouse clicks) 
and controls the vertices.
After the user clicks a vertex, the board will activate boost::breadth_first_search
to find all the vertices that are connected to the source.

--- boost::breadth_search _first ---
gets a graph and a source vertex (which is the middle of the vector always).
boost needs some kind of Visitor object in order to do operations when a vertex is met
during the search.

--- Board To Visitor Proxy ---
BFSVisitor (my custom boost::bfs_visitor) gets the BoardToVisitorProxy which in turn
holds a reference to the Board. When the bfs meets a vertex, the BFSVisitor asks
the Proxy to locate that vertex in the array (actually, the visitor gets a number as the
vertex descriptor), the order of the vertices in my vector is the same order I added them
to the boost graph so actually that number that boost gives me is the vertex index in my vector..
anyway, I use the proxy to light that vertex.

--- Logger ---
Is a Singleton responsible for logging errors to a log file.
It's a singleton because I want it available everywhere and of course 
I would want one log file...

--- Controller ---
just runs a loop, and asks the LevelBuilder for the next level..



5.--------------- LIST OF FILES -----------------

* BFSVisitor.h .cpp
  derived from boost::default_bfs_visitor.
  Custom Visitor Object that boost uses when a vertex is met in bfs.
  I made it light the lights (giving it access to the Board Vertex vector).

* Board.h .cpp
  Holds the vertices vector and boost::graph. updates them when the user
  clicks a light.

* BoardToVisitorProxy.h .cpp
  Board Proxy with access to the vertices vector. Used by BFSVisitor
  to light the lights during bfs.

* Controller.h .cpp
  Runs the game loop.

* definitions.h 
  Some global definitions that are used by different parts of the program.

* EdgeLineException.h .cpp
  Derived from std::runtime_error.
  thrown when there illegal input in the edges lines in the level file.

* LevelBuilder.h .cpp
  sSingleton that parses the level files and builds boards.

* LevelSizeException.h .cpp
  Derived from std::runtime_error.
  Thrown when the level size line ( the first line) in the file in illegal.

* Logger.h .cpp
  Singleton that logs error to a log file.

* NeighborProxy.h .cpp
  A vertex Proxy that a vertex gives it's neighbors so they could
  communicate with it.

* Vertex.h .cpp
  The light bulb.



6.--------------- DATA STRUCTURES -----------------

* Proxies:
  NeighborProxy and BoardToVisitorProxy.

* LevelBuilder and Logger are Singletons. 



7. --------------- NOTABLE ALGORITHMS -----------------

* std::rotate to rotate a bool deque.

* boost::breadth_first_search to find the vertices that are connected to the source
  and light them.



8. ------------------- KNOWN BUGS ---------------------


